<?php

namespace App\Http\Controllers;

use App\Category;
use App\Product;
use Illuminate\Http\Request;

class MainController extends Controller
{
    public function index() {
        $products = Product::paginate(6);
        return view('index', ['products' => $products]);
    }

    public function categories() {
        $categories = Category::get();
        return view('categories', ['categories' => $categories]);
    }

    public function category($code) {
        $category = Category::where('code', $code)->first();
        return view('category', ['category' => $category]);
    }

    public function product($category, $product) {
        return view('product', ['name' => $product]);
    }
}
