@extends("layout.main")

@section('title', 'Categories')

@section('content')
    @foreach($categories as $category)
        <div class="panel">
            <a href='{{ route('category', $category->code) }}'>
                <img src="http://internet-shop.tmweb.ru/storage/categories/mobile.jpg1">
                <h2>{{ $category->name }}</h2>
            </a>
            <p>
                {{ $category->description }}
            </p>
        </div>
    @endforeach
@endsection
