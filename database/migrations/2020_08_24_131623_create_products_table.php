<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;
use \Illuminate\Support\Facades\DB;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->bigInteger('category_id');
            $table->string('name');
            $table->string('code');
            $table->text('description')->nullable();
            $table->text('image')->nullable();
            $table->double('price')->default(0);
            $table->timestamps();
        });

        DB::table('products')->insert([
            [
                'category_id' => '1',
                'name' => 'iPhone X 64GB',
                'code' => 'mobiles',
                'description' => 'Отличный продвинутый телефон с памятью на 64 gb',
                'price' => '71990'
            ],
            [
                'category_id' => '1',
                'name' => 'iPhone X 256GB',
                'code' => 'mobiles',
                'description' => 'Отличный продвинутый телефон с памятью на 256 gb',
                'price' => '89990'
            ],
            [
                'category_id' => '1',
                'name' => 'HTC One S',
                'code' => 'mobiles',
                'description' => 'Зачем платить за лишнее? Легендарный HTC One S',
                'price' => '12490'
            ],
            [
                'category_id' => '1',
                'name' => 'iPhone 5SE',
                'code' => 'mobiles',
                'description' => 'Отличный классический iPhone',
                'price' => '17221'
            ],
            [
                'category_id' => '2',
                'name' => 'Наушники Beats Audio',
                'code' => 'portable',
                'description' => 'Отличный звук от Dr. Dre',
                'price' => '20221'
            ],
            [
                'category_id' => '2',
                'name' => 'Камера GoPro',
                'code' => 'portable',
                'description' => 'Снимай самые яркие моменты с помощью этой камеры',
                'price' => '12000'
            ],
            [
                'category_id' => '2',
                'name' => 'Камера Panasonic HC-V770',
                'code' => 'portable',
                'description' => 'Для серьёзной видео съемки нужна серьёзная камера. Panasonic HC-V770 для этих целей лучший выбор!',
                'price' => '27900'
            ],
            [
                'category_id' => '3',
                'name' => 'Кофемашина DeLongi',
                'code' => 'appliances',
                'description' => 'Хорошее утро начинается с хорошего кофе!',
                'price' => '25200'
            ],
            [
                'category_id' => '3',
                'name' => 'Холодильник Haier',
                'code' => 'appliances',
                'description' => 'Для большой семьи большой холодильник!',
                'price' => '40200'
            ],
            [
                'category_id' => '3',
                'name' => 'Блендер Moulinex',
                'code' => 'appliances',
                'description' => 'Для самых смелых идей',
                'price' => '4200'
            ],
            [
                'category_id' => '3',
                'name' => 'Мясорубка Bosch',
                'code' => 'appliances',
                'description' => 'Любите домашние котлеты? Вам определенно стоит посмотреть на эту мясорубку!',
                'price' => '9200'
            ]
        ]);
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
